package com.quarkus.database.example.repository.interfaces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.quarkus.database.example.model.Book;

import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;

@ApplicationScoped
public class RepositoryImpl implements Repository<Book> {

  private final String INSERT_QUERY = "INSERT INTO books(id,name,description,author) VALUES(null,?,?,?)";
  private final String UPDATE_QUERY = "UPDATE  books SET name=?,description=?,author=? WHERE id=?";
  private final String DELETE_QUERY = "DELETE FROM books where id=?";
  private final String SELECT_ALL_QUERY = "SELECT * FROM books";
  private final String SELECT_BY_ID_QUERY = "SELECT * FROM books where id=?";

  @Inject
  @DataSource("library")
  AgroalDataSource dataSource;

  @Override
  public Book get(int id) throws SQLException {
    Book book = null;
    try (Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement(SELECT_BY_ID_QUERY);) {
      stmt.setInt(1, id);
      try (ResultSet rs = stmt.executeQuery();) {
        while (rs.next()) {
          book = new Book(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("author"));
        }
      }
    } catch (SQLException e) {
      throw new SQLException("Get book failed.");
    }
    return book;
  }

  @Override
  public List<Book> getAll() throws SQLException {
    List<Book> books = new ArrayList<>();
    try (Connection connection = dataSource.getConnection();
        PreparedStatement stmt = connection.prepareStatement(SELECT_ALL_QUERY);) {
      try (ResultSet rs = stmt.executeQuery();) {
        while (rs.next()) {
          books.add(
              new Book(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getString("author")));
        }
      }
    } catch (SQLException e) {
      throw new SQLException("Get books failed.");

    }
    return books;
  }

  @Override
  public void insert(Book book) throws SQLException {
    try (PreparedStatement ps = dataSource.getConnection().prepareStatement(INSERT_QUERY, new String[] { "id" })) {
      ps.setString(1, book.getName());
      ps.setString(2, book.getDescription());
      ps.setString(3, book.getAuthor());
      int numRowsAffected = ps.executeUpdate();

      if (numRowsAffected == 0) {
        throw new SQLException("Creating book failed, no rows affected.");
      }
      try (ResultSet rs = ps.getGeneratedKeys()) {
        if (rs.next()) {
          book.setId(Integer.parseInt(rs.getString(1)));
        }
      } catch (SQLException s) {
        throw new SQLException("Creating book failed, no ID obtained.");
      }
    } catch (SQLException e) {
      throw new SQLException("Creating book failed.");
    }

  }

  @Override
  public void update(int id, Book book) throws SQLException {
    try (PreparedStatement ps = dataSource.getConnection().prepareStatement(UPDATE_QUERY)) {
      ps.setString(1, book.getName());
      ps.setString(2, book.getDescription());
      ps.setString(3, book.getAuthor());
      ps.setInt(4, id);

      int numRowsAffected = ps.executeUpdate();

      if (numRowsAffected == 0) {
        throw new SQLException("Update book failed, no rows affected.");
      }

    } catch (SQLException e) {
      throw new SQLException("update book failed.");
    }

  }

  @Override
  public void delete(int id) throws SQLException {
    try (PreparedStatement ps = dataSource.getConnection().prepareStatement(DELETE_QUERY)) {
      ps.setInt(1, id);

      int numRowsAffected = ps.executeUpdate();

      if (numRowsAffected == 0) {
        throw new SQLException("Delete book failed, no rows affected.");
      }

    } catch (SQLException e) {
      throw new SQLException("Delete book failed.");
    }

  }

}