package com.quarkus.database.example;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.quarkus.database.example.model.Book;
import com.quarkus.database.example.service.LibraryService;

@Path("/library")
public class LibraryResource {

  @Inject
  LibraryService service;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/all")
  public Response getAll() {
    try {
      List<Book> books = service.getAll();
      return Response.ok(books).build();
    } catch (SQLException e) {
      return Response.status(400).build();
    }
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/get/{id}")
  public Response get(@PathParam("id") int id) {
    try {
      Book book = service.get(id);
      return Response.ok(book).build();
    } catch (SQLException e) {
      return Response.status(400).build();
    }
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/insert")
  public Response insert(Book book) {
    try {
      service.insert(book);
      return Response.ok("insert ok!").build();
    } catch (SQLException e) {
      return Response.status(400).build();
    }
  }

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/update/{id}")
  public Response insert(@PathParam("id") int id, Book book) {
    try {
      service.update(id, book);
      return Response.ok("update ok!").build();
    } catch (SQLException e) {
      return Response.status(400).build();
    }
  }

  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/delete/{id}")
  public Response insert(@PathParam("id") int id) {
    try {
      service.delete(id);
      return Response.ok("delete ok!").build();
    } catch (SQLException e) {
      return Response.status(400).build();
    }
  }
}