package com.quarkus.database.example.service;

import java.sql.SQLException;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.quarkus.database.example.model.Book;
import com.quarkus.database.example.repository.interfaces.Repository;

@ApplicationScoped
public class LibraryService {

  @Inject
  Repository<Book> repository;

  public List<Book> getAll() throws SQLException {
    return repository.getAll();
  }

  public Book get(int id) throws SQLException {
    return repository.get(id);
  }

  public void insert(Book book) throws SQLException {
    repository.insert(book);
  }

  public void update(int id, Book book) throws SQLException {
    repository.update(id, book);
  }

  public void delete(int id) throws SQLException {
    repository.delete(id);
  }
}